const express         = require('express')
const path            = require('path')
const cookieParser    = require('cookie-parser')
const bodyParser      = require('body-parser')
const env             = require('./commons/env')
const cors            = require('cors')
const jwtauth         = require('./middlewares/jwtauth')

/* ROUTERS */
const users           = require('./routes/users')
const employees       = require('./routes/employees')
const company         = require('./routes/company')
const trucks          = require('./routes/trucks')
const contracts       = require('./routes/contracts')
const expenses        = require('./routes/expenses')
const revenues        = require('./routes/revenues')
const notifications   = require('./routes/notifications')
const loans           = require('./routes/loans')
const dashboard       = require('./routes/dashboard')
const locations       = require('./routes/locations')
const attachments     = require('./routes/attachment')
const roles           = require('./routes/roles')

const app             = express()

require('./commons/db')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(cors())

app.use('/api/public', express.static(__dirname + '/public'));

app.use('/api/users', users) // auth configured in file routes
app.use('/api/employees', jwtauth, employees)
app.use('/api/trucks', jwtauth, trucks)
app.use('/api/contracts', jwtauth, contracts)
app.use('/api/expenses', jwtauth, expenses)
app.use('/api/company', company)
app.use('/api/revenues', jwtauth, revenues)
app.use('/api/loans', jwtauth, loans)
app.use('/api/notifications', jwtauth, notifications)
app.use('/api/dashboard', jwtauth, dashboard)
app.use('/api/locations', locations)
app.use('/api/attachments', jwtauth, attachments)
app.use('/api/roles', roles)

module.exports = app
