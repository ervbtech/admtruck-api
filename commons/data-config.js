'use strict';

var Brazil      = require('../models/domains/brazil')
, Country       = require('../models/country')
, State         = require('../models/state')
, City          = require('../models/city')
, countries     = [
  Brazil.get()
]
, cfg = {};

cfg.configureDomains = function configureDomains() {
  for(var i = 0; i < countries.length; i++) {
    var country = countries[i]
    var entity = JSON.parse(JSON.stringify(country))
    entity.states = []
    Country.save(entity, country.states, { name: country.name})
      .then((states) => {
        saveStates(states)
      })
      .catch((err) => {
        console.log(err)
      })
  }
}

var saveStates = (states) => {
  for(var i = 0; i < states.length; i++) {
    var state = states[i]
    var entity = JSON.parse(JSON.stringify(state))
    entity.cities = []
    State.save(entity, state.cities, { name: state.name})
      .then((cities) => {
        saveCities(cities)
      })
      .catch((err) => {
        console.log(err)
      })
  }
}

var saveCities = (cities) => {
  for(var i = 0; i < cities.length; i++) {
    var city = cities[i]
    if(city) {
      City.save(city, { name: city.name})
      .catch((err) => {
        console.log(err)
      })
    }
  }
}

module.exports = cfg;
