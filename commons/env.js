'use strict'

const ENV = {}

ENV.APP_NAME = 'admtruck-api'

ENV.SERVER = {}
ENV.SERVER.PORT = parseInt(process.env.SERVER_PORT || 3000)

ENV.UPLOAD_DIR = process.env.UPLOAD_PATH || __dirname + '/../public'

ENV.JWT_KEY_TOKEN = Buffer.from('45726b264d69682431383131', 'hex') // Token Key

ENV.DATABASE = {}
// ENV.DATABASE.URI = 'mongodb://admtruck_dbmanager:Erk&Mih$1811@ds145178.mlab.com:45178/admtruck'
ENV.DATABASE.URI = 'mongodb://localhost/admtruck'
ENV.DATABASE.CREDENTIAL = {
  user: process.env.DATABASE_USER || '',
  pass: process.env.DATABASE_PASS || ''
}

module.exports = ENV;
