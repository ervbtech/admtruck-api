'use strict'

const mongoose  = require('mongoose')
const bluebird  = require('bluebird')
const debug     = require('debug')('database')
const env       = require('./env').DATABASE

// configure a mongoose connection
mongoose.Promise = bluebird
mongoose.connect(env.URI, env.CREDENTIAL)

const db = mongoose.connection

// configure connection listeners
db.on('connected', () => {
  debug('MongoDB connected')
})

db.on('open', () => {
  // db.db.dropDatabase()
  debug('MongoDB open')
})

db.on('disconnected', () => {
  debug('MongoDB disconnected')
})

db.on('error', err => {
  debug(`MongoDB error: ${err}`)
})

// configure process listener
process.on('SIGINT', () => {
  mongoose.connection.close(() => {
    debug('MongoDB disconnected through app termination')
    process.exit(0)
  })
})

module.exports = db
