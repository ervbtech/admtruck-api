'use strict'
const mailer  = require("nodemailer");
const options =  {
    transporter: 'smtps://noreply%40admtruck.com.br:senha%40123@smtp.zoho.com'
}
module.exports.send = (mailFrom, mailTo, subject, message, isHtml) => {
  return new Promise((resolve, reject) => {
    let _mail = {
      from: mailFrom
    , to: mailTo
    , subject: subject
    };

    if(isHtml) {
      _mail.html = message
    } else {
      _mail.text = message
    }

    mailer.createTransport(options.transporter).sendMail(_mail, (err) => {
      if(err) {
        reject(err)
      } else {
        resolve()
      }
    })

  })
}
