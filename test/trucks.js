'use strict'

/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   This file configure test unit for Truck operations
*/

const expect    = require('chai').expect
const request   = require('request')
const faker     = require('faker')
const uri       = 'http://localhost:3000/'
const trucksApi   = uri + 'trucks'

faker.locale = 'pt_BR'

let Truck = {
    name: faker.name.findName()
  , model: faker.name.findName()
  , year: faker.random.number()
  , plate: faker.random.word()
  , renavam: faker.lorem.word()
  , status: 'active'
}

describe('Trucks operations ', () => {
  describe('Insert Truck', () => {
    it('should returns truck created data', (done) => {
      request
        .post(trucksApi)
        .form(Truck)
        .on('data', (data) => {
          Truck = JSON.parse(data)
          Truck.name = 'Scania'
          expect(data.length).to.be.above(0)
          done()
        })
    })
  })

  describe('Update Truck', () => {
    it('should return truck updated data', (done) => {
      request
        .put(trucksApi + '/' + Truck._id)
        .form(Truck)
        .on('data', (data) => {
          let truck = JSON.parse(data)
          expect(truck.name).to.equal('Scania')
          done()
        })
    })
  })

  describe('Find All Trucks', () => {
    it('should return trucks data list', (done) => {
      request
        .get(trucksApi)
        .on('data', (data) => {
          expect(data.length).to.be.above(0)
          done()
        })
    })
  })

  describe('Find One Truck', () => {
    it('should return trucks data list', (done) => {
      request
        .get(trucksApi + '/' + Truck._id)
        .on('data', (data) => {
          let truck = JSON.parse(data)
          expect(truck._id).to.not.equal(undefined)
          done()
        })
    })
  })

  describe('Delete Trucj', () => {
    it('should remove truck and return statusCode 200', (done) => {
      request(trucksApi + '/' + Truck._id)
      .on('response', (response) => {
        expect(response.statusCode).to.equal(200)
        done()
      })
    })
  })
})
