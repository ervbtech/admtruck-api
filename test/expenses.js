'use strict'

/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   This file configure test unit for Expense operations
*/


const expect      = require('chai').expect
const request     = require('request')
const faker       = require('faker')
const uri         = 'http://localhost:3000/'
const expenseApi  = uri + 'expenses'

let Expense = {
      value: faker.finance.amount()
    , date: faker.date.recent()
    , type: 'operational'
    , invoiceNumber: faker.random.number()
    , description: faker.lorem.sentences()
};

describe('Expense operations', () => {

  describe('Create Expense', () => {
    it('should returns expense data', (done) => {
      request
        .post(expenseApi)
        .form(Expense)
        .on('data', (data) => {
          expect(data.length).to.be.above(0) // expected data is not empty
          Expense = JSON.parse(data)
          Expense.invoiceNumber = 11234568
          done()
        })
    })
  })

  describe('Update Expense', () => {
    it('should returns expense update data', (done) => {
      request
        .put(expenseApi + '/' + Expense._id)
        .form(Expense)
        .on('response', (response) => {
          response.on('data', (data) => {
            let expense = JSON.parse(data)
            expect(expense.invoiceNumber).to.equal(11234568)
            done()
          })
        })
    })
  })

  describe('Find All Expense', () => {
      it('returns status 200', (done) => {
        request
          .get(expenseApi)
          .on('response', (response) => {
            expect(response.statusCode).to.equal(200)
            done()
          })
      })
  })

  describe('Find One Expense', () => {
      it('find only one expenses', (done) => {
        request
          .get(expenseApi + '/' + Expense._id)
          .on('response', (response) => {
            expect(response.statusCode).to.equal(200)
            done()
          })
      })
  })

  describe('Delete expense data', () => {
    it('should remove expense by id', (done) => {
      request
        .delete(expenseApi + '/' + Expense._id)
        .on('response', (response) => {
          expect(response.statusCode).to.equal(200)
          done()
        })
    })
  })
})
