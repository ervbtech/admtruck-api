'use strict'

/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   This file configure test unit for Employee operations
*/


const expect    = require('chai').expect
const request   = require('request')
const faker     = require('faker')
const uri       = 'http://localhost:3000/'
const employeesApi   = uri + 'employees'

faker.locale = 'pt_BR'

let Employee = {
    name: faker.name.findName()
  , cpf: faker.random.number()
  , rg: faker.random.number()
  , driverLicense: faker.random.number()
  , birthDate: faker.date.past()
  , phone: faker.phone.phoneNumberFormat()
  , occupation: faker.commerce.department()
  , email: faker.internet.email()
  , address: faker.address.streetAddress()
  , photo: faker.image.image()
}

describe('Employees operations ', () => {
  describe('Insert Employee', () => {
    it('should returns employee created data', (done) => {
      request
        .post(employeesApi)
        .form(Employee)
        .on('data', (data) => {
          Employee = JSON.parse(data)
          Employee.name = 'Joaquim Santos'
          expect(data.length).to.be.above(0)
          done()
        })
    })
  })

  describe('Update Employee', () => {
    it('should return employee updated data', (done) => {
      request
        .put(employeesApi + '/' + Employee._id)
        .form(Employee)
        .on('data', (data) => {
          let employee = JSON.parse(data)
          expect(employee.name).to.equal('Joaquim Santos')
          done()
        })
    })
  })

  describe('Find All Employees', () => {
    it('should return employees data list', (done) => {
      request
        .get(employeesApi)
        .on('data', (data) => {
          expect(data.length).to.be.above(0)
          done()
        })
    })
  })

  describe('Find One Employee', () => {
    it('should return employees data list', (done) => {
      request
        .get(employeesApi + '/' + Employee._id)
        .on('data', (data) => {
          let employee = JSON.parse(data)
          expect(employee._id).to.not.equal(undefined)
          done()
        })
    })
  })

  describe('Delete Employee', () => {
    it('should remove employee and return statusCode 200', (done) => {
      request(employeesApi + '/' + Employee._id)
      .on('response', (response) => {
        expect(response.statusCode).to.equal(200)
        done()
      })
    })
  })
})
