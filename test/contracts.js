'use strict'

/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   This file configure test unit for Contract operations
*/

const expect    = require('chai').expect
const request   = require('request')
const faker     = require('faker')
const uri       = 'http://localhost:3000/'
const contractsApi   = uri + 'contracts'

faker.locale = 'pt_BR'

let Contract = {
      name: faker.name.findName()
    , description: faker.lorem.sentences()
    , executionDate: faker.date.past()
    , contractingCompany: faker.company.companyName()
    , cnpjContractingCompany: faker.company.companySuffix()
    , attachments: [
          { name: 'Contrato de Celebração.pdf' }
        , { name: 'Contrato Social.docx' }
        , { name: 'Procuração.png' }
        , { name: 'Tabela de Valores.xls' }
    ]
}

describe('Contracts operations ', () => {
  describe('Insert Contract', () => {
    it('should returns contract created data', (done) => {
      request
        .post(contractsApi)
        .form(Contract)
        .on('data', (data) => {
          Contract = JSON.parse(data)
          Contract.name = 'Scania'
          expect(data.length).to.be.above(0)
          done()
        })
    })
  })

  describe('Update Contract', () => {
    it('should return contract updated data', (done) => {
      request
        .put(contractsApi + '/' + Contract._id)
        .form(Contract)
        .on('data', (data) => {
          let contract = JSON.parse(data)
          expect(contract.name).to.equal('Scania')
          done()
        })
    })
  })

  describe('Find All Contracts', () => {
    it('should return contracts data list', (done) => {
      request
        .get(contractsApi)
        .on('data', (data) => {
          expect(data.length).to.be.above(0)
          done()
        })
    })
  })

  describe('Find One Contract', () => {
    it('should return contracts data list', (done) => {
      request
        .get(contractsApi + '/' + Contract._id)
        .on('data', (data) => {
          let contract = JSON.parse(data)
          expect(contract._id).to.not.equal(undefined)
          done()
        })
    })
  })

  describe('Delete Contract', () => {
    it('should remove contract and return statusCode 200', (done) => {
      request(contractsApi + '/' + Contract._id)
      .on('response', (response) => {
        expect(response.statusCode).to.equal(200)
        done()
      })
    })
  })
})
