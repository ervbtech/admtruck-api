'use strict'

/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   This file configure test unit for Loan operations
*/


const expect      = require('chai').expect
const request     = require('request')
const faker       = require('faker')
const uri         = 'http://localhost:3000/'
const loansApi  = uri + 'loans'

let Loan = {
      value: faker.finance.amount()
    , date: faker.date.recent()
    , type: 'operational'
    , invoiceNumber: faker.random.number()
    , description: faker.lorem.sentences()
};

describe('Loan operations', () => {

  describe('Create Loan', () => {
    it('should returns loans data', (done) => {
      request
        .post(loansApi)
        .form(Loan)
        .on('data', (data) => {
          expect(data.length).to.be.above(0) // expected data is not empty
          Loan = JSON.parse(data)
          Loan.invoiceNumber = 11234568
          done()
        })
    })
  })

  describe('Update Loan', () => {
    it('should returns loans update data', (done) => {
      request
        .put(loansApi + '/' + Loan._id)
        .form(Loan)
        .on('response', (response) => {
          response.on('data', (data) => {
            let loans = JSON.parse(data)
            expect(loans.invoiceNumber).to.equal(11234568)
            done()
          })
        })
    })
  })

  describe('Find All Loan', () => {
      it('returns status 200', (done) => {
        request
          .get(loansApi)
          .on('response', (response) => {
            expect(response.statusCode).to.equal(200)
            done()
          })
      })
  })

  describe('Find One Loan', () => {
      it('find only one loans', (done) => {
        request
          .get(loansApi + '/' + Loan._id)
          .on('response', (response) => {
            expect(response.statusCode).to.equal(200)
            done()
          })
      })
  })

  describe('Delete loans data', () => {
    it('should remove loans by id', (done) => {
      request
        .delete(loansApi + '/' + Loan._id)
        .on('response', (response) => {
          expect(response.statusCode).to.equal(200)
          done()
        })
    })
  })
})
