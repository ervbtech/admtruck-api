'use strict'

/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   This file configure test unit for Notification operations
*/

const expect    = require('chai').expect
const request   = require('request')
const faker     = require('faker')
const uri       = 'http://localhost:3000/'
const notificationsApi   = uri + 'notifications'

faker.locale = 'pt_BR'

let Notification = {
      message: faker.lorem.sentences()
    , date: faker.date.past()
    , read: faker.random.boolean()
}

describe('Notifications operations ', () => {
  describe('Insert Notification', () => {
    it('should returns notification created data', (done) => {
      request
        .post(notificationsApi)
        .form(Notification)
        .on('data', (data) => {
          Notification = JSON.parse(data)
          Notification.message = 'Updated Message'
          expect(data.length).to.be.above(0)
          done()
        })
    })
  })

  describe('Update Notification', () => {
    it('should return notification updated data', (done) => {
      request
        .put(notificationsApi + '/' + Notification._id)
        .form(Notification)
        .on('data', (data) => {
          let notification = JSON.parse(data)
          expect(notification.message).to.equal('Updated Message')
          done()
        })
    })
  })

  describe('Find All Notifications', () => {
    it('should return notifications data list', (done) => {
      request
        .get(notificationsApi)
        .on('data', (data) => {
          expect(data.length).to.be.above(0)
          done()
        })
    })
  })

  describe('Find One Notification', () => {
    it('should return notifications data list', (done) => {
      request
        .get(notificationsApi + '/' + Notification._id)
        .on('data', (data) => {
          let notification = JSON.parse(data)
          expect(notification._id).to.not.equal(undefined)
          done()
        })
    })
  })

  describe('Delete Trucj', () => {
    it('should remove notification and return statusCode 200', (done) => {
      request(notificationsApi + '/' + Notification._id)
      .on('response', (response) => {
        expect(response.statusCode).to.equal(200)
        done()
      })
    })
  })
})
