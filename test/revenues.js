'use strict'

/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   This file configure test unit for Revenues operations
*/


const expect      = require('chai').expect
const request     = require('request')
const faker       = require('faker')
const uri         = 'http://localhost:3000/'
const revenuesApi  = uri + 'revenues'

let Revenues = {
      value: faker.finance.amount()
    , date: faker.date.recent()
    , type: 'operational'
    , invoiceNumber: faker.random.number()
    , description: faker.lorem.sentences()
};

describe('Revenues operations', () => {

  describe('Create Revenues', () => {
    it('should returns revenues data', (done) => {
      request
        .post(revenuesApi)
        .form(Revenues)
        .on('data', (data) => {
          expect(data.length).to.be.above(0) // expected data is not empty
          Revenues = JSON.parse(data)
          Revenues.invoiceNumber = 11234568
          done()
        })
    })
  })

  describe('Update Revenues', () => {
    it('should returns revenues update data', (done) => {
      request
        .put(revenuesApi + '/' + Revenues._id)
        .form(Revenues)
        .on('response', (response) => {
          response.on('data', (data) => {
            let revenues = JSON.parse(data)
            expect(revenues.invoiceNumber).to.equal(11234568)
            done()
          })
        })
    })
  })

  describe('Find All Revenues', () => {
      it('returns status 200', (done) => {
        request
          .get(revenuesApi)
          .on('response', (response) => {
            expect(response.statusCode).to.equal(200)
            done()
          })
      })
  })

  describe('Find One Revenues', () => {
      it('find only one revenues', (done) => {
        request
          .get(revenuesApi + '/' + Revenues._id)
          .on('response', (response) => {
            expect(response.statusCode).to.equal(200)
            done()
          })
      })
  })

  describe('Delete revenues data', () => {
    it('should remove revenues by id', (done) => {
      request
        .delete(revenuesApi + '/' + Revenues._id)
        .on('response', (response) => {
          expect(response.statusCode).to.equal(200)
          done()
        })
    })
  })
})
