'use strict'

/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   This file configure test unit for User operations
*/


const expect  = require('chai').expect
const request = require('request')
const uri     = 'http://localhost:3000/'
const userApi = uri + 'users'

let User = {
    displayName: 'Administrator'
  , login: 'admin'
  , email: 'admin@gmail.com'
};

describe('User operations', () => {

  describe('Create User', () => {
    it('should returns user data', (done) => {
      request
        .post(userApi)
        .form(User)
        .on('data', (data) => {
          expect(data.length).to.be.above(0) // expected data is not empty
          User = JSON.parse(data)
          User.login = 'erickbarbosa'
          done()
        })
    })
  })

  describe('Update User', () => {
    it('should returns user update data', (done) => {
      request
        .put(userApi + '/' + User._id)
        .form(User)
        .on('response', (response) => {
          response.on('data', (data) => {
            let user = JSON.parse(data)
            expect(user.login).to.equal('erickbarbosa')
            done()
          })
        })
    })
  })

  describe('Find All User', () => {
      it('returns status 200', (done) => {
        request
          .get(userApi)
          .on('response', (response) => {
            expect(response.statusCode).to.equal(200)
            done()
          })
      })
  })

  describe('Find One User', () => {
      it('find only one users', (done) => {
        let login = 'admin'
        request
          .get(userApi + '/' + login)
          .on('response', (response) => {
            expect(response.statusCode).to.equal(200)
            done()
          })
      })
  })

  describe('Delete user data', () => {
    it('should remove user by login', (done) => {
      let login = 'erickbarbosa'
      request
        .delete(userApi + '/' + login)
        .on('response', (response) => {
          expect(response.statusCode).to.equal(200)
          done()
        })
    })
  })
})
