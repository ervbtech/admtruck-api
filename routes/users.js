  'use strict'

/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   This file configure routes to expose endpoints for user services
*/

const express = require('express')
const jwt     = require('jwt-simple')
const moment  = require('moment')
const bcrypt  = require('bcrypt-nodejs')
const router  = express.Router()
const jwtauth = require('../middlewares/jwtauth')
const User    = require('../models/users')
const Company    = require('../models/company')
const env     = require('../commons/env')
const sender  = require('../utils/sender')
const fs      = require('fs')
const app     = express()

router.get('/', (req, res, next) => {
  User.find()
  .then((data) => {
    res.json(data)
  })
  .catch((err) => {
    next(err)
  })
})

router.get('/:email', (req, res, next) => {
  User.findOne({ email: req.params.email })
  .then((data) => {
    res.json(data)
  })
  .catch((err) => {
    next(err)
  })
})

router.post('/signin', (req, res, next) => {
  User.findOne({ email: req.body.email, active: true })
  .then((data) => {
    if(data) {
      let hash = data.password
      let expires = moment().add(30, 'minutes').valueOf()
      bcrypt.compare(req.body.password, hash, function(err, result) {
        if(result) {
          delete data.password
          let webToken = jwt.encode({data: data, company: data.company, exp: expires}, env.JWT_KEY_TOKEN, false, 'HS512')
          res.json({ token: webToken })
        } else {
          res.sendStatus(400)
        }
      })
    } else {
      res.sendStatus(404)
    }
  })
  .catch((err) => {
    next(err)
  })
})

router.post('/newsletter', (req, res, next) => {
  fs.readFile(require('path').resolve(__dirname, '../resources/mails/newsletter.html'), 'utf8', (err, data) => {
    if(!err) {
      sender.send('noreply@admtruck.com.br', 'contact@admtruck.com.br', 'Organize Seu Negócio', data.replace('{0}', req.body.email), true)
      .then(data => {
          res.send('Email sended success');
      });
    }
  })
});

router.post('/signup', (req, res, next) => {
  fs.readFile(require('path').resolve(__dirname, '../resources/mails/signup-email.html'), 'utf8', (err, data) => {
    if(!err) {
        sender.send('noreply@admtruck.com.br', 'contact@admtruck.com.br', 'Novo cadastro', data.replace('{0}', req.body.email), true)
        .then(data => {
          // Send to Client
          fs.readFile(require('path').resolve(__dirname, '../resources/mails/free-newsletter-template/markup/index.html'), 'utf8', (err, data) => {
            if(!err) {
                sender.send('noreply@admtruck.com.br', req.body.email, 'Cadastro Realizado', data, true)
                .then(data => {
                  res.send('Email sended success')
                })
                .catch((err) => {
                  res.status(500).send(err)
                })
            } else {
              res.status(404).send(err)
            }
          });
        })
        .catch((err) => {
          res.status(500).send(err)
        })
    } else {
      res.status(404).send(err)
    }
  });


})

router.post('/', /*jwtauth,*/ (req, res, next) => {
  let user = req.body
  let password = user.password
  // hash value for one way
  user.password = bcrypt.hashSync(user.password)

  User.save({}, req.body)
  .then((data) => {
    res.json(data)
  })
  .catch((err) => {
    next(err)
  })
})

router.put('/:id'/*, jwtauth*/, (req, res, next) => {
  User.save({ _id: req.params.id }, req.body)
  .then((data) => {
    res.json(data)
  })
  .catch((err) => {
    next(err)
  })
})

router.delete('/:login'/*, jwtauth*/, (req, res, next) => {
  User.delete(req.params.login)
  .then((data) => {
    res.json(data).status(200)
  })
  .catch((err) => {
    next(err)
  })
})

module.exports = router
