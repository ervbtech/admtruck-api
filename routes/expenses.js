'use strict'

/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   This file configure routes to expose endpoints for expense services
*/

const express  = require('express')
const router   = express.Router()
const Expense  = require('../models/expenses')
const moment        = require('moment')

router.get('/', (req, res, next) => {
  let q = req.headers['company'] ? req.headers['company'] : null
  Expense.find({ company: q })
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.get('/:id', (req, res, next) => {
  Expense.findOne({ _id: req.params.id })
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.post('/', (req, res, next) => {
  Expense.save({}, req.body)
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.put('/:id', (req, res, next) => {
  Expense.save({ _id: req.params.id }, req.body)
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.delete('/:id', (req, res, next) => {
  Expense.delete(req.params.id)
    .then((data) => {
      res.json().status(200)
    })
    .catch((err) => {
      next(err)
    })
})

router.get('/test/go', (req, res, next) => {
    Expense.find({ date: { $gte: moment().toDate(), $lte: moment().add(2, 'days').toDate() } })
    .then((data) => {
      let notifications = []
      for (var i = 0; i < data.length; i++) {
        let expense = data[i]
        notifications.push({
          title: 'Despesa à vencer'
          , message: 'A seguinte Despesa vence no dia ' + moment().add(1, 'days').format('DD/MM/YYYY') + ': ' + expense.description
          , date: moment().toDate()
          , link: expense._id
          , read: false
        })
      }
      res.json(notifications)
    })
    .catch(err => {
      next(err)
    })
})

module.exports = router
