'use strict'

/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   This file configure routes to expose endpoints for loans services
*/

const express  = require('express')
const router   = express.Router()
const Loans  = require('../models/loans')

router.get('/', (req, res, next) => {
  let q = req.headers['company'] ? req.headers['company'] : null
  Loans.find({ company: q })
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.get('/:id', (req, res, next) => {
  Loans.findOne({ _id: req.params.id })
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.post('/', (req, res, next) => {
  Loans.save({}, req.body)
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      console.log(err);
      next(err)
    })
})

router.put('/:id', (req, res, next) => {
  Loans.save({ _id: req.params.id }, req.body)
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.delete('/:id', (req, res, next) => {
  Loans.delete(req.params.id)
    .then((data) => {
      res.json().status(200)
    })
    .catch((err) => {
      next(err)
    })
})

module.exports = router
