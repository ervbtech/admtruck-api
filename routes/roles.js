'use strict'

const express = require('express')
const Role   = require('../models/role')
const router  = express.Router()

router.get('/', (req, res, next) => {
   Role.find()
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.get('/:id', (req, res, next) => {
  Role.findOne({ _id: req.params.id })
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.post('/', (req, res, next) => {
  Role.save({}, req.body)
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.put('/:id', (req, res, next) => {
  Role.save({ _id: req.params.id }, req.body)
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.delete('/:id', (req, res, next) => {
  Role.delete(req.params.id)
    .then((data) => {
      res.json().status(200)
    })
    .catch((err) => {
      next(err)
    })
})

module.exports = router
