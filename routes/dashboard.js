'use strict'

/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   This file configure routes to expose endpoints for employee services
*/

const express   = require('express')
const router    = express.Router()
const Dashboard = require('../models/dashboard')
const moment    = require('moment')

moment.locale('pt-BR')

router.get('/contract/revenues/:filter', (req, res, next) => {
  let q = req.headers['company'] ? req.headers['company'] : null
  Dashboard.getContractRevenuesDataChart({ company: q, date: req.params.filter })
  .then((data) => {
    let total = 0
    let result = []
    for(var i = 0; i < data.length; i++) {
      total += data[i].value
    }
    for(var i = 0; i < data.length; i++) {
      var row = data[i]
      result.push({
        name: row._id,
        progress: ((row.value * 100) / total).toFixed(2),
        value: row.value
      })
    }
    res.json(result)
  })
  .catch((err) => {
    next(err)
  })
})

router.get('/contract/expenses/:filter', (req, res, next) => {
  let q = req.headers['company'] ? req.headers['company'] : null
  Dashboard.getContractExpensesDataChart({ company: q, date: req.params.filter })
  .then((data) => {
    let total = 0
    let result = []
    for(var i = 0; i < data.length; i++) {
      total += data[i].value
    }
    for(var i = 0; i < data.length; i++) {
      var row = data[i]
      result.push({
        name: row._id,
        progress: ((row.value * 100) / total).toFixed(2),
        value: row.value
      })
    }
    res.json(result)
  })
  .catch((err) => {
    next(err)
  })
})

router.get('/income/:filter', (req, res, next) => {
  let q = req.headers['company'] ? req.headers['company'] : null
  Dashboard.getIncomeDataChart({ company: q, year: req.params.filter.substr(0, 4) })
  .then((data) => {
    let labels = [], values = []
    for(var i = 0; i < data.length; i++) {
      let row = data[i]
      labels.push(moment(row._id).format('MMMM'))
      values.push(row.value)
    }
    res.json({
      labels: labels
      , values: values
    })
  })
  .catch((err) => {
    next(err)
  })
})

router.get('/contributiontruck/:filter', (req, res, next) => {
  let q = req.headers['company'] ? req.headers['company'] : null
  Dashboard.getContributionTruckDataChart({ company: q, date: req.params.filter })
  .then((data) => {
    let labels = [], values = []
    for(var i = 0; i < data.length; i++) {
      let row = data[i]
      labels.push(row._id.plate)
      values.push(row.value)
    }
    res.json({
      labels: labels
      , values: values
    })
  })
  .catch((err) => {
    next(err)
  })
})

router.get('/truckexpenses/:filter', (req, res, next) => {
  let q = req.headers['company'] ? req.headers['company'] : null
  Dashboard.getTruckExpensesDataChart({ company: q, date: req.params.filter })
  .then((data) => {
    let labels = [], values = []
    for(var i = 0; i < data.length; i++) {
      let row = data[i]
      labels.push(row._id.plate)
      values.push(row.value)
    }
    res.json({
      labels: labels
      , values: values
    })
  })
  .catch((err) => {
    next(err)
  })
})
router.get('/production/:filter', (req, res, next) => {
  let q = req.headers['company'] ? req.headers['company'] : null
  Dashboard.getProductionDataChart({company: q, date: req.params.filter })
  .then((data) => {
    let labels = [], values = [[],[]]

    for(var i = 0; i < data.length; i++) {
      let row = data[i]
      let truck = row._id
      labels.push(truck.name + ' - ' + truck.model + ' ('+ truck.plate +')')
      values[0].push(row.estimatedProduction)
      values[1].push(row.total)
    }
    res.json({
      labels: labels
      , values: values
    })
  })
  .catch((err) => {
    next(err)
  })
})

router.get('/header/:filter', (req, res, next) => {

  let q = req.headers['company'] ? req.headers['company'] : null
  Dashboard.getHeaderTotals({ company: q, date: req.params.filter })
  .then((data) => {
    let result = {
        revenues: 0
      , revenuesLast: 0
      , expenses: 0
      , expensesLast: 0
      , totalAccount: 0
      , totalAccountLast: 0
    }
    let expensesData = data[0]
        , revenuesData = data[1]

    if(expensesData.length > 1) {
      result.expensesLast = expensesData[0].value
      result.expenses = expensesData[1].value
    } else if(expensesData.length) {
      if(moment(req.params.filter).month() + 1 == expensesData[0]._id.month)
        result.expenses = expensesData[0].value
      else
        result.expensesLast = expensesData[0].value
    }

    if(revenuesData.length > 1) {
        result.revenuesLast = revenuesData[0].value
        result.revenues = revenuesData[1].value
    } else if(revenuesData.length) {
      if(moment(req.params.filter).month() + 1 == revenuesData[0]._id.month)
        result.revenues = revenuesData[0].value
      else
        result.revenuesLast = revenuesData[0].value
    }

    res.json({
      totalEarnings: result.revenues
      , totalExpense: result.expenses
      , profit: result.revenues - result.expenses
      , totalCash: result.totalAccount
      , percentEarningsLast: getDiffPercent(result.revenues, result.revenuesLast)
      , percentExpenseLast: getDiffPercent(result.expenses, result.expensesLast)
      , percentProfitLast:  getDiffPercent((result.revenues - result.expenses), (result.revenuesLast - result.expensesLast))
      , percentTotalCashLast: getDiffPercent(result.totalAccount, result.totalAccountLast)
    })
  })
  .catch((err) => {
    next(err)
  })
})

router.get('/cashflow/:year', (req, res, next) => {
  let q = req.headers['company'] ? req.headers['company'] : null
  Dashboard.getCashFlowPresentation({ company: q, year: req.params.year })
  .then((data) => {
    // res.json(data)
    let primary   = fillCashFlowMonths(data[0])
    let secondary = fillCashFlowMonths(data[1])
    let third     = fillCashFlowMonths(data[2])

    let total     = JSON.parse(JSON.stringify(primary))

    for(let i = 0; i < total.length; i++) {
      let pr = total[i]
      for(let j = 0; j < secondary.length; j++) {
        let sr = secondary[j]
        if(moment(pr._id).month() == moment(sr._id).month()) {
          pr.value = (sr.value - pr.value)
        }
      }
    }
    res.json([primary, secondary, third, total]) // send updated array
  })
  .catch((err) => {
    next(err)
  })
})

function fillCashFlowMonths(array) {
  let result = []
  for(let i = 0; i < 12; i++) {
    let newRow = { _id: { month: i + 1 }, value: 0 }
    for(let j = 0; j < array.length; j++) {
      let row = array[j]
      if(moment(row._id).month() === i) {
        newRow.value = row.value
        break
      }
    }
    result.push(newRow)
  }
  return result
}

function getDiffPercent(a, b) {
    return b > 0 ? parseFloat((a - b) / b).toFixed(2): a / 100
}

module.exports = router
