'use strict'

/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   This file configure routes to expose endpoints for expense services
*/

const express  = require('express')
const router   = express.Router()
const Country  = require('../models/country')
const State    = require('../models/state')
const City     = require('../models/city')

router.get('/countries', (req, res, next) => {
  Country.find({})
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.get('/countries/:id', (req, res, next) => {
  Country.find({ _id: req.params.id })
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.get('/states', (req, res, next) => {
  State.find()
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.get('/cities', (req, res, next) => {
  City.find({})
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.get('/states/:country', (req, res, next) => {
  State.find({ country: req.params.country })
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.get('/cities/:state', (req, res, next) => {
  City.find({ state: req.params.state })
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

module.exports = router
