'use strict'

/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   This file configure routes to expose endpoints for attachments services
*/
const express     = require('express')
const https       = require('https')
const fs          = require('fs')
const mv          = require('mv')
const path        = require('path')
const env         = require('../commons/env')
const multiparty  = require('connect-multiparty')
const router      = express.Router()

router.post('/upload', multiparty(), (req, res, next) => {
  if(req.files.file !== undefined) {
    var file = req.files.file;
    var _uploadDir = path.join(env.UPLOAD_DIR, '/images');
    // check existis uploads directory
    fs.stat(_uploadDir, function(err, stats) {
      if(err || !stats.isDirectory()) {
        fs.mkdirSync(_uploadDir);
      }
      mv(file.path, _uploadDir + '/' + file.name, (err) => {
        if (err) { throw err; }
        res.sendStatus(201);
      });
    });
  } else {
    next()
  }
})

module.exports = router
