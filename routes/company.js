'use strict'

/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   This file configure routes to expose endpoints for Company services
*/

const express  = require('express')
const router   = express.Router()
const Company = require('../models/company')

router.get('/', (req, res, next) => {
  Company.findAll()
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.get('/:id', (req, res, next) => {
  Company.findOne({ _id: req.params.id })
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.post('/', (req, res, next) => {
  Company.save({}, req.body)
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.put('/:id', (req, res, next) => {
  Company.save({ _id: req.params.id }, req.body)
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.delete('/:id', (req, res, next) => {
  Company.delete(req.params.id)
    .then((data) => {
      res.json().status(200)
    })
    .catch((err) => {
      next(err)
    })
})

module.exports = router
