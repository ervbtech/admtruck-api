'use strict'

/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   This file configure routes to expose endpoints for employee services
*/

const express  = require('express')
const router   = express.Router()
const Employee = require('../models/employees')

router.get('/', (req, res, next) => {
  let q = req.headers['company'] ? req.headers['company'] : null
  Employee.find({ company: q })
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.get('/:id', (req, res, next) => {
  Employee.findOne({ _id: req.params.id })
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.post('/', (req, res, next) => {
  Employee.save({}, req.body)
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.put('/:id', (req, res, next) => {
  Employee.save({ _id: req.params.id }, req.body)
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.delete('/:id', (req, res, next) => {
  Employee.delete(req.params.id)
    .then((data) => {
      res.json().status(200)
    })
    .catch((err) => {
      next(err)
    })
})

module.exports = router
