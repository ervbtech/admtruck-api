'use strict'

/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   This file configure routes to expose endpoints for contract services
*/

const mapsAPI  = 'maps.googleapis.com'
const API_KEY  = 'AIzaSyBd5s-wv6FSPKbTWk1bL48HeALFhyvi40o'

const express  = require('express')
const https    = require('https')
const router   = express.Router()
const Contract = require('../models/contracts')
const Revenues = require('../models/revenues')

router.post('/budget', (req, res, next) => {
  let params = req.body // {fromState, fromCity, toState, toCity, fuelPrice, averageConsumption, totalValue}
  let resp = res
  let options = {
    host: mapsAPI,
    port: 443,
    path: '/maps/api/distancematrix/json?origins='+encodeURIComponent(params.placeFrom)+'&destinations='+encodeURIComponent(params.placeTo)+'&language=pt-BR&key='+API_KEY,
    headers: {
      'Content-Type': 'application/json'
    }
  }

  https.get(options, res => {
    res.on('data', data => {
      let dm = JSON.parse(data).rows[0].elements[0]

      /* CALC BUDGET HERE */
      let totalExpense = (Math.round(dm.distance.value / 1000) / params.averageConsumption) * params.fuelPrice

      for(var i = 0; i < params.costs.length; i++) {
        let cost = params.costs[i]
        totalExpense += cost.value
      }

      resp.json({
        distance: dm.distance.text
        , duration: dm.duration.text
        , totalValue: params.totalValue
        , totalExpense: totalExpense
        , profit: params.totalValue - totalExpense
        , budget: params
      })
    })
  }).on('error', e => {
    next(e)
  })

})

router.get('/', (req, res, next) => {
  let q = req.headers['company'] ? req.headers['company'] : null
  let $param = {company: q};
  Revenues.getContributionPercentageContract($param)
  .then((cpcdata) => {
    let percentages = []
    let total = 0
    for (var i = 0; i < cpcdata.length; i++) {
      let percentage = cpcdata[i]
      if(percentage) {
        total += percentage.value
        if(percentage._id) {
          percentages[percentage._id] = percentage.value
        }
      }
    }
    Contract.findAll($param)
    .then((data) => {
      for (var i = 0; i < data.length; i++) {
        let contract = data[i]
        let index = percentages.indexOf(contract._id)
        contract.contributionPercentage = percentages[contract._id] ? ((percentages[contract._id] * 100) / total).toFixed(2) : 0
      }
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
  })
  .catch((err) => {
    next(err)
  })
})

router.get('/:id', (req, res, next) => {
  Contract.findOne({ _id: req.params.id })
  .then((data) => {
    res.json(data)
  })
  .catch((err) => {
    next(err)
  })
})

router.get('/trucks/:id', (req, res, next) => {
  Contract.findOne({ _id: req.params.id })
  .then((data) => {
    res.json(data.trucks)
  })
  .catch((err) => {
    next(err)
  })
})

router.post('/filter', (req, res, next) => {
  let _query = req.body
  let q = req.headers['company'] ? req.headers['company'] : null
  _query.company = q
  Contract.findAll(_query)
  .then((data) => {
    res.json(data)
  })
  .catch((err) => {
    next(err)
  })
})

router.post('/', (req, res, next) => {
  Contract.save({}, req.body)
  .then((data) => {
    res.json(data)
  })
  .catch((err) => {
    next(err)
  })
})

router.put('/:id', (req, res, next) => {
  Contract.save({ _id: req.params.id }, req.body)
  .then((data) => {
    res.json(data)
  })
  .catch((err) => {
    next(err)
  })
})

router.delete('/:id', (req, res, next) => {
  Contract.delete(req.params.id)
  .then((data) => {
    res.json().status(200)
    module.exports = router

  })
  .catch((err) => {
    next(err)
  })
})

module.exports = router
