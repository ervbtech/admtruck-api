'use strict'

/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   This file configure routes to expose endpoints for revenues services
*/

const express  = require('express')
const router   = express.Router()
const Revenues  = require('../models/revenues')

router.get('/', (req, res, next) => {
  let q = req.headers['company'] ? req.headers['company'] : null
  Revenues.find({ company: q })
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.get('/:id', (req, res, next) => {
  Revenues.findOne({ _id: req.params.id })
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.post('/', (req, res, next) => {
  Revenues.save({}, req.body)
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      console.log(err);
      next(err)
    })
})

router.put('/:id', (req, res, next) => {
  Revenues.save({ _id: req.params.id }, req.body)
    .then((data) => {
      res.json(data)
    })
    .catch((err) => {
      next(err)
    })
})

router.delete('/:id', (req, res, next) => {
  Revenues.delete(req.params.id)
    .then((data) => {
      res.json().status(200)
    })
    .catch((err) => {
      next(err)
    })
})

module.exports = router
