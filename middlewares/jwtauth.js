'use strict'

const jwt = require('jwt-simple')
const env = require('../commons/env')

module.exports = (req, res, next) => {
  let token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'] || req.headers['authorization'] || req.headers['Authorization']
  if (token) {
    try {
      token = token.replace('Bearer ', '')
      var decoded = jwt.decode(token, env.JWT_KEY_TOKEN)
      if (decoded.exp <= Date.now()) {
        // res.status(400).send('Access token has expired')
      }
      next()
    } catch (err) {
      res.status(404).send('Error when parse token')
    }
  } else {
    res.status(403).send('No access token provided')
  }
}
