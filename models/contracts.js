/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   Contract Model provides functions for contract manipulation data
*/

'use strict'

const mongoose = require('mongoose')
const Schema   = mongoose.Schema({
  name: { type: String, required: true, trim: true }
  , description: { type: String, trim: true }
  , date: { type: Date, required: true }
  , employees: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Employee' }]
  , trucks: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Truck'}]
  , locationFrom: { type: [Number], index: '2dsphere'}
  , locationTo: { type: [Number], index: '2dsphere'}
  , estimatedProduction: { type: Number, require: true }
  , contractingCompany: { type: String, required: true, trim: true }
  , cnpjContractingCompany: { type: String, required: true, trim: true }
  , attachments: [{ type: mongoose.Schema.Types.Mixed, default: [] }]
  , active: { type: Boolean, required: true, default: false }
  , company: { type: mongoose.Schema.Types.ObjectId, ref: 'Company', required: true }
})

const Contract = mongoose.model('Contract', Schema)

module.exports.findAll = (query) => {
  return Contract.find(query).populate('employees trucks').lean().exec()
}

module.exports.findOne = (query) => {
  return Contract.findOne(query).populate('employees trucks').lean().exec()
}

module.exports.save = (query, entity) => {
  if(query._id) {
    delete entity._id
    return Contract.findOneAndUpdate(query, entity, { new: true })
  } else {
    return Contract.create(entity)
  }
}

module.exports.delete = (id) => {
  return Contract.remove({ _id: id })
}
