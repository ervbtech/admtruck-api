'use stric'

const mongoose = require('mongoose')
const Schema   = mongoose.Schema({
  value: { type: Number, required: true }
  , date: { type: Date, required: true }
  , invoiceNumber: { type: Number }
  , description: { type: String, trim: true, required: true }
  , contract: { type: mongoose.Schema.Types.ObjectId, ref: 'Contract' }
  , truck: { type: mongoose.Schema.Types.ObjectId, ref: 'Truck', required: true }
  , company: { type: mongoose.Schema.Types.ObjectId, ref: 'Company', required: true }
})
const Revenues = mongoose.model('Revenues', Schema)

module.exports.find = (_query) => {
  return Revenues.find(_query).populate('contract truck').lean().exec()
}

module.exports.findOne = (query) => {
  return Revenues.findOne(query).lean().exec()
}

module.exports.save = (query, entity) => {
  if(query._id) {
    delete entity._id
    return Revenues.findOneAndUpdate(query, entity, { new: true })
  } else {
    return Revenues.create(entity)
  }
}

module.exports.delete = (id) => {
  return Revenues.remove({ _id: id })
}

module.exports.getTotal = ($param) => {
  return Revenues.aggregate([
    {
      $project: {
        value: '$value'
        , day: { $dayOfMonth: "$date" }
        , month: { $month: "$date" }
        , year: { $year: "$date" }
        , date: '$date'
        , company: '$company'
      }
    }
    , { $match: {
      $and: [
        { year: parseInt($param.date.substr(0, 4)) }
        , { $or: [
          {month: parseInt($param.date.substr(4, 2))}
          ,{month: parseInt($param.date.substr(4, 2)) - 1}
        ] }
        , { company: mongoose.Types.ObjectId($param.company) }
      ]
    }
  }
  , { $sort: { date: -1 } }
  , {
    $group: {
      _id:  { month: { $month: '$date'} },
      value: {
        $sum: '$value'
      }
    }
  }
])
}

module.exports.getCashFlow = ($param) => {
  return Revenues.aggregate([
    {
      $project: {
        year: { $year: "$date" }
        , value: '$value'
        , date: '$date'
        , company: '$company'
      }
    }
    , { $match : {
      $and: [
        { year: parseInt($param.year) },
        { company: mongoose.Types.ObjectId($param.company) }
      ]
    }
  }
  , { $sort: { date: -1 } }
  , {
    $group: {
      _id: '$date'
      , value: {
        $sum: '$value'
      }
    }
  }
])
}

module.exports.getContributionPercentageContract = ($param) => {
  return Revenues.aggregate([
    {
      $project: {
        contract: '$contract'
        , value: '$value'
        , year: { $year: '$date'}
        , company: '$company'
      }
    }, {
      $match: { company: mongoose.Types.ObjectId($param.company) }
    }, {
      $group: {
        _id: '$contract'
        , value: {
          $sum: '$value'
        }
      }
    }
  ])
}

module.exports.getContributionTruckDataChart = ($param) => {
  return Revenues.aggregate([
    {
      $lookup: {
        from: "trucks",
        localField: "truck",
        foreignField: "_id",
        as: "truck"
      }
    }
    , {
      $project: {
        year: { $year: "$date" }
        , month: { $month: "$date" }
        , truck: {  $arrayElemAt: [ '$truck', 0 ] }
        , value: '$value'
        , date: '$date'
        , company: '$company'
      }
    }, {
      $match: {
        $and: [
          { company: mongoose.Types.ObjectId($param.company) }
          , { year: parseInt($param.date.substr(0, 4)) }
          , { month: parseInt($param.date.substr(4, 2))}
        ]
      }
    }
    , {
      $group: {
        _id: '$truck'
        , value: {
          $sum: '$value'
        }
      }
    }
  ])
}

module.exports.getContractRevenuesDataChart = ($param) => {
  return Revenues.aggregate([
    {
      $lookup: {
        from: "contracts",
        localField: "contract",
        foreignField: "_id",
        as: "contract"
      }
    }, {
      $project: {
        contract: {  $arrayElemAt: [ '$contract', 0 ] },
        year: { $year: "$date" },
        month: { $month: "$date" },
        done: '$contract.done',
        value: '$value',
        company: '$company'

      }
    }, {
      $match: {
        $and: [
          { year: parseInt($param.date.substr(0, 4)) }
          , { month: parseInt($param.date.substr(4, 2))}
          , { done: true }
          , { company: mongoose.Types.ObjectId($param.company) }
        ]
      }
    }, {
      $group: {
        _id: '$contract.name',
        value: {
          $sum: '$value'
        }
      }
    }
  ])
}


module.exports.getProductionDataChart = ($param) => {
  return Revenues.aggregate([
    {
      $lookup: {
        from: "trucks",
        localField: "truck",
        foreignField: "_id",
        as: "truck"
      }
    }, {
      $lookup: {
        from: "contracts",
        localField: "contract",
        foreignField: "_id",
        as: "contract"
      }
    }, {
      $project: {
        contract: {  $arrayElemAt: [ '$contract', 0 ] }
        , truck: {  $arrayElemAt: [ '$truck', 0 ] }
        , month: { $month: "$date" }
        , year: { $year: "$date" }
        , company: '$company'
      }
    }, {
      $match: {
        $and: [
          { year: parseInt($param.date.substr(0, 4)) }
          , { month: parseInt($param.date.substr(4, 2))}
          , { company: $param.company }
        ]
      }
    }, {
      $group: {
        _id: '$truck'
        , estimatedProduction: { $sum: '$contract.estimatedProduction' }
        , total: { $sum: 1 }
      }
    }
  ])
}
