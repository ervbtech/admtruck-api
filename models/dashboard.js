'use strict'

const Expense   = require('./expenses')
const Revenues  = require('./revenues')
const Loans     = require('./loans')
const Contract  = require('./contracts')

module.exports.getCashFlowPresentation = ($param) => {
  return  Promise.all([Expense.getCashFlow($param), Revenues.getCashFlow($param), Loans.getCashFlow($param)])
}

module.exports.getHeaderTotals = ($param) => {

  let _promises = [
      Expense.getTotal($param)
    , Revenues.getTotal($param)
  ]

  return Promise.all(_promises)

}

module.exports.getIncomeDataChart = ($param) => {
  return Revenues.getCashFlow($param)
}

module.exports.getProductionDataChart = ($param) => {
  return Revenues.getProductionDataChart($param)
}

module.exports.getContributionTruckDataChart = ($param) => {
  return Revenues.getContributionTruckDataChart($param)
}

module.exports.getTruckExpensesDataChart = ($param) => {
  return Expense.getTruckExpensesDataChart($param)
}

module.exports.getContractExpensesDataChart = ($param) => {
  return Expense.getContractExpensesDataChart($param)
}

module.exports.getContractRevenuesDataChart = ($param) => {
  return Revenues.getContractRevenuesDataChart($param)
}
