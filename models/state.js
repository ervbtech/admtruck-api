/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   State Model provides functions for state manipulation data
*/

'use strict'

const mongoose = require('mongoose')
const Schema   = mongoose.Schema({
    name: { type: String, required: true, trim: true },
    abbreviation: { type: mongoose.Schema.Types.String, required: true, trim: true },
    country: { type: mongoose.Schema.Types.ObjectId, ref: 'Country', required: true }
})

const State = mongoose.model('State', Schema)

module.exports.find = (query) => {
  return State.find(query).sort('name').lean().exec()
}


module.exports.findOne = (query) => {
  return State.findOne(query).lean().exec()
}

/**
* Save state and return your cities
*/
module.exports.save = (entity, cities, query) => {
  delete entity._id
  return new Promise((resolve, reject) => {
     State.findOneAndUpdate(query, entity, { new: true, upsert: true })
      .then((data) => {
        for(var i = 0; i < cities.length; i++) {
          var city = cities[i]
          if(city)
            city.state = data._id
        }
        resolve(cities)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

module.exports.delete = (id) => {
  return State.remove({ _id: id })
}
