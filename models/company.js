/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   Company Model provides functions for contract manipulation data
*/

'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema({
    name: { type: String, required: true, trim: true },
    photo: { type: String },
    phone: { type: String, required: true },
    cnpj: { type: String, required: true, trim: true },
    active: { type: Boolean, required: true, default: false }
})

const Company = mongoose.model('Company', Schema)

module.exports.findAll = () => {
  return  Company.find({}).lean().exec()
}

module.exports.findOne = (query) => {
  return Company.findOne(query).lean().exec()
}

module.exports.save = (query, entity) => {
  if(query._id) {
    delete entity._id;
    return Company.findOneAndUpdate(query, entity, {new: true})
  } else {
    return Company.create(entity)
  }
}

module.exports.delete = (email) => {
  return Company.remove({email: email})
}
