'use strict'

const mongoose = require('mongoose')
const Schema   = mongoose.Schema({
    title: { type: String, trim: true }
  , message: { type: String, require: true, trim: true }
  , link: { type: mongoose.Schema.Types.ObjectId, required: true }
  , date: { type: Date, require: true }
  , eventDate: { type: Date, require: true }
  , read: { type: Boolean, require: true, default: false }
  , company: { type: mongoose.Schema.Types.ObjectId, ref: 'Company', required: true }
})
const Notification = mongoose.model('Notification', Schema)

module.exports.find = (query) => {
  return Notification.find(query).lean().exec()
}

module.exports.findOne = (query) => {
  return Notification.findOne(query).lean().exec()
}

module.exports.save = (query, entity) => {
  if(query._id) {
    delete entity._id
    return Notification.findOneAndUpdate(query, entity, { new: true })
  } else {
    return Notification.create(entity)
  }
}

module.exports.delete = (id) => {
  return Notification.remove({ _id: id })
}
