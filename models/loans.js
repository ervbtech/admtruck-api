'use stric'

const mongoose = require('mongoose')
const Schema   = mongoose.Schema({
    value: { type: Number, required: true }
  , date: { type: Date, required: true }
  , contractNumber: { type: Number, required: true }
  , parcel: { type: Number, required: true }
  , tax: { type: Number, required: true }
  , description: { type: String, trim: true, required: true }
  , company: { type: mongoose.Schema.Types.ObjectId, ref: 'Company', required: true }
})
const Loans = mongoose.model('Loans', Schema)

module.exports.find = (query) => {
  return Loans.find(query).lean().exec()
}

module.exports.findOne = (query) => {
  return Loans.findOne(query).lean().exec()
}

module.exports.save = (query, entity) => {
  if(query._id) {
    delete entity._id
    return Loans.findOneAndUpdate(query, entity, { new: true })
  } else {
    return Loans.create(entity)
  }
}

module.exports.delete = (id) => {
  return Loans.remove({ _id: id })
}

module.exports.getCashFlow = ($param) => {
  return Loans.aggregate([
        {
          $project: {
              year: { $year: "$date" }
            , value: '$value'
            , date: '$date'
            , company: '$company'
          }
        }
      , { $match : {
          $and: [
            { year: parseInt($param.year) }
            , { company: mongoose.Types.ObjectId($param.company) }
          ]
        }
      }
      , { $sort: { date: -1 } }
      , {
          $group: {
             _id: '$date',
             value: {
               $sum: '$value'
             }
          }
     }
  ])
}
