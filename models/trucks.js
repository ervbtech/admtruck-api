/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   Truck Model provides functions for truck manipulation data
*/

'use strict'

const mongoose = require('mongoose')
const Schema   = mongoose.Schema({
    name: { type: String, required: true, trim: true }
  , model: { type: String, required: true, trim: true }
  , year: { type: Number, required: true }
  , plate: { type: String, required: true, trim: true }
  , renavam: { type: String, required: true, trim: true }
  , status: { type: String, enum: ['under_maintenance', 'sold', 'active', 'awaiting_allocation'], required: true }
  , company: { type: mongoose.Schema.Types.ObjectId, ref: 'Company', required: true }
  , forAggregation: { type: Boolean, required: true, default: false }
  , participationExpense: { type: Boolean, required: true, default: true }
  , participationPercentage: { type: Number, require: true, default: 1 }
})
const Truck = mongoose.model('Truck', Schema)

module.exports.findAll = () => {
  return Truck.find().lean().exec()
}

module.exports.find = (_query) => {
  return Truck.find(_query).lean().exec()
}

module.exports.findOne = (query) => {
  return Truck.findOne(query).lean().exec()
}

module.exports.save = (query, entity) => {
  if(query._id) {
    delete entity._id
    return Truck.findOneAndUpdate(query, entity, { new: true })
  } else {
    return Truck.create(entity)
  }
}

module.exports.delete = (id) => {
  return Truck.remove({ _id: id })
}
