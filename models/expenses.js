'use stric'

const mongoose = require('mongoose')
const Schema   = mongoose.Schema({
  value: { type: Number, required: true }
  , date: { type: Date, required: true }
  , type: { type: String, enum: ['taxable', 'operational', 'eventual', 'loan'], required: true }
  , invoiceNumber: { type: Number }
  , description: { type: String, trim: true, required: true }
  , status: { type: String, enum: ['opened', 'closed']}
  , loan: { type: mongoose.Schema.Types.ObjectId, ref: 'Loans' }
  , contract: { type: mongoose.Schema.Types.ObjectId, ref: 'Contract' }
  , truck: { type: mongoose.Schema.Types.ObjectId, ref: 'Truck' }
  , employee: { type: mongoose.Schema.Types.ObjectId, ref: 'Employee' }
  , company: { type: mongoose.Schema.Types.ObjectId, ref: 'Company', required: true }
})
const Expense = mongoose.model('Expense', Schema)

module.exports.find = (query) => {
  return Expense.find(query).populate('contract truck employee').lean().exec()
}

module.exports.findOne = (query) => {
  return Expense.findOne(query).lean().exec()
}

module.exports.save = (query, entity) => {
  if(query._id) {
    delete entity._id
    return Expense.findOneAndUpdate(query, entity, { new: true })
  } else {
    return Expense.create(entity)
  }
}

module.exports.delete = (id) => {
  return Expense.remove({ _id: id })
}

module.exports.getTotal = ($param) => {
  return Expense.aggregate([
    {
      $project: {
        value: '$value'
        , day: { $dayOfMonth: "$date" }
        , month: { $month: "$date" }
        , year: { $year: "$date" }
        , date: '$date'
        , company: '$company'
      }
    }
    , { $match: {
      $and: [
        { year: parseInt($param.date.substr(0, 4)) }
        , { $or: [
              {month: parseInt($param.date.substr(4, 2))}
              ,{month: parseInt($param.date.substr(4, 2)) - 1}
            ]
          }
        , { company: mongoose.Types.ObjectId($param.company) }
      ]
    }
  }
  , { $sort: { date: -1 } }
  , {
    $group: {
      _id:  { month: { $month: '$date'} },
      value: {
        $sum: '$value'
      }
    }
  }
])
}

module.exports.getCashFlow = ($param) => {
  return Expense.aggregate([
    {
      $project: {
        year: { $year: "$date" }
        , value: '$value'
        , date: '$date'
        , company: '$company'
      }
    }
    , { $match : {
      $and: [
        { year: parseInt($param.year) }
        , { company: mongoose.Types.ObjectId($param.company) }
      ]
    }
  } , { $sort: { date: -1 } }
  , {
    $group: {
      _id: '$date',
      value: {
        $sum: '$value'
      }
    }
  }
])
}

module.exports.getTruckExpensesDataChart = ($param) => {
  return Expense.aggregate([
    {
      $lookup: {
        from: "trucks",
        localField: "truck",
        foreignField: "_id",
        as: "truck"
      }
    }
    , {
      $project: {
        year: { $year: "$date" }
        , month: { $month: "$date" }
        , truck: {  $arrayElemAt: [ '$truck', 0 ] }
        , value: '$value'
        , date: '$date'
        , company: '$company'
      }
    }, {
      $match: {
        $and: [
          { year: parseInt($param.date.substr(0, 4)) }
          , { truck: { $exists:true } }
          , { company: mongoose.Types.ObjectId($param.company) }
        ]
      }
    }
    , {
      $group: {
        _id: '$truck'
        , value: {
          $sum: '$value'
        }
      }
    }
  ])
}

module.exports.getContractExpensesDataChart = ($param) => {
  return Expense.aggregate([
    {
      $lookup: {
        from: "contracts",
        localField: "contract",
        foreignField: "_id",
        as: "contract"
      }
    }, {
      $project: {
        contract: {  $arrayElemAt: [ '$contract', 0 ] },
        year: { $year: "$date" },
        month: { $month: "$date" },
        done: '$contract.done',
        value: '$value',
        company: '$company'

      }
    }, {
      $match: {
        $and: [
          { year: parseInt($param.date.substr(0, 4)) }
          , { month: parseInt($param.date.substr(4, 2))}
          , { company: mongoose.Types.ObjectId($param.company) }
        ]
      }
    }, {
      $group: {
        _id: '$contract.name',
        value: {
          $sum: '$value'
        }
      }
    }
  ])
}
