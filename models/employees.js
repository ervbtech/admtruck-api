/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   Employee Model provides functions for employee manipulation data
*/

'use strict'

const mongoose = require("mongoose")
const Schema   = mongoose.Schema({
    name: { type: String, required: true, unique: true }
  , nickname: { type: String, trim: true }
  , cpf: { type: String, required: true, trim: true, unique: true }
  , rg: { type: String, required: true, trim: true, unique: true }
  , driverLicense: { type: String, required: true, trim: true, unique: true }
  , birthDate: { type: Date, required: true }
  , phone: { type: String }
  , occupation: { type: String, required: true }
  , email: { type: String }
  , address: { type: String }
  , description: { type: String }
  , company: { type: mongoose.Schema.Types.ObjectId, ref: 'Company', required: true }
})
const Employee = mongoose.model('Employee', Schema)

module.exports.find = (_query) => {
  return Employee.find(_query).lean().exec()
}

module.exports.findOne = (query) => {
  return Employee.findOne(query).lean().exec()
}

module.exports.save = (query, entity) => {
  if(query._id) {
    delete entity._id
    return Employee.findOneAndUpdate(query, entity, { new: true })
  } else {
    return Employee.create(entity)
  }
}

module.exports.delete = (id) => {
  return Employee.remove({ _id: id })
}
