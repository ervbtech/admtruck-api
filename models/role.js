/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   Role Model provides functions for roles manipulation data
*/

'use strict'

const mongoose = require('mongoose')
const Schema   = mongoose.Schema({
    name: { type: String, required: true }
  , active: { type: Boolean, required: true, default: true }
})

const Role = mongoose.model('Role', Schema);

module.exports.find = (_query) => {
  return Role.find(_query).lean().exec()
}

module.exports.findOne = (query) => {
  return Role.findOne(query).lean().exec()
}

module.exports.save = (query, entity) => {
  if(query._id) {
    delete entity._id;
    return Role.findOneAndUpdate(query, entity, {new: true})
  } else {
    return Role.create(entity)
  }
}

module.exports.delete = (email) => {
  return Role.remove({email: email})
}
