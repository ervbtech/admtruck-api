/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   City Model provides functions for City manipulation data
*/

'use strict'

const mongoose = require('mongoose')
const Schema   = mongoose.Schema({
    name: { type: String, required: true, trim: true },
    state: { type: mongoose.Schema.Types.ObjectId, ref: 'State', required: true }
})

const City = mongoose.model('City', Schema)

module.exports.find = (query) => {
  return City.find(query).sort('name').lean().exec()
}

module.exports.findOne = (query) => {
  return City.findOne(query).lean().exec()
}

module.exports.save = (entity, query) => {
  delete entity._id
  return City.findOneAndUpdate(query, entity, { new: true, upsert: true })
}

module.exports.delete = (id) => {
  return City.remove({ _id: id })
}
