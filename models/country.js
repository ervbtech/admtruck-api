/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   Country Model provides functions for country manipulation data
*/

'use strict'

const mongoose = require('mongoose')
const Schema   = mongoose.Schema({
  name: { type: String, required: true, trim: true }
})

const Country = mongoose.model('Country', Schema)

module.exports.find = (query) => {
  return Country.find(query).lean().exec()
}

module.exports.findOne = (query) => {
  return Country.findOne(query).lean().exec()
}

/**
* Save country and return your states
*/
module.exports.save = (entity, states, query) => {
  delete entity._id
  return new Promise((resolve, reject) => {
     Country.findOneAndUpdate(query, entity, { new: true, upsert: true })
      .then((data) => {
        for(var i = 0; i < states.length; i++) {
          var state = states[i]
          state.country = data._id
        }
        resolve(states)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

module.exports.delete = (id) => {
  return Country.remove({ _id: id })
}
