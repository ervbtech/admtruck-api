/**
*   @author Erick Ricardo Vieira Barbosa (erickbbosa@gmail.com)
*
*   User Model provides functions for user manipulation data
*/

'use strict'

const mongoose  = require('mongoose')
const Schema   = new mongoose.Schema({
      displayName: { type: String, unique: true, required: true }
    , email: { type: String, unique: true, required: true }
    , password: { type: String, require: true }
    , photo: { type: String }
    , active: { type: Boolean, required: true, default: false }
    , company: { type: mongoose.Schema.Types.ObjectId, ref: 'Company', required: true }
    , role: { type: mongoose.Schema.Types.ObjectId, ref: 'Role', required: true }
});
const User = mongoose.model('User', Schema)

module.exports.find = (_query) => {
  return  User.find(_query).populate('company role').lean().exec()
}

module.exports.findOne = (query) => {
  return User.findOne(query).populate('company role').lean().exec()
}

module.exports.save = (query, entity) => {
  if(query._id) {
    delete entity._id;
    return User.findOneAndUpdate(query, entity, {new: true})
  } else {
    return User.create(entity)
  }
}

module.exports.delete = (email) => {
  return User.remove({email: email})
}
